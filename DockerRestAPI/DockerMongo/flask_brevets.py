"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request, Flask, redirect, url_for, request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient


import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient('172.18.0.2', 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    # flask.session['timeslink'] = flask.url_for("times")
    return flask.render_template('calc.html')

@app.route("/times")
def times():
    app.logger.debug("Times page entry")
    _items = db.tododb.find()
    items = [item for item in _items]
    # db.tododb.remove({})
    return flask.render_template('times.html', items=items)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    brevet_distance = request.args.get('distance', type=float)
    begin_date = request.args.get('begin_date', type=str)
    # begin_date = arrow.get(begin_date, "YYYY-MM-DD")
    begin_time = request.args.get('begin_time', type=str)
    # begin_time = arrow.get(begin_time, "HH:mm")
    date_time = begin_date + " " + begin_time
    date_time = arrow.get(date_time, "YYYY-MM-DD HH:mm")
    


    open_time = acp_times.open_time(km, brevet_distance, date_time)
    close_time = acp_times.close_time(km, brevet_distance, date_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

# database
@app.route("/_submit_times")
def _submit_times():
    # store the times in the database.
    # open_close_times = {
    #     'open': request.args.get('open_time'),
    #     'close': request.args.get('close_time')
    # }
    miles = request.args.get('miles')
    open_time = request.args.get('open_time')
    close_time = request.args.get('close_time')

    db.tododb.insert_one({'miles': miles, 'open': open_time, 'close': close_time})
    return ""


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
