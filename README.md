# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

Author: Dylan Conway Email: dconway@uoregon.edu

## What is in this repository

You have a minimal implementation of Docker compose in DockerRestAPI folder, using which you can create REST API-based services (as demonstrated in class). 

